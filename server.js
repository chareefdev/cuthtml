const watch = require('node-watch');
const express = require('express')
const app = express()
const port = 8080
const http = require('http').Server(app);
const io = require('socket.io')(http);


app.use('/', express.static(process.cwd()))


http.listen(port, function () {
    console.log(`port ${port}`);
});

io.on('connection', function (socket) {
    console.log('a user connected');
});

watch(process.cwd(), { recursive: true }, function () {
    console.log('change')
    io.emit('change',{ for: 'everyone' })
})